package com.safebear.app.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by CCA_Student on 23/04/2018.
 */
public class WelcomePage {

    //create empty variable for webdriver
    WebDriver driver;
    //tell Selenium how to find variable
    @FindBy(linkText = "Login")
    WebElement loginLink;

    //constructor to populate driver variable
    public WelcomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    //check we're on the right page
    public boolean checkCorrectPage() {
        return driver.getTitle().startsWith("Welcome");
    }

    //create a method to click on the LogIn link
    public void clickOnLogin() {
        loginLink.click();
    }
}
