package com.safebear.app.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by CCA_Student on 23/04/2018.
 */
public class UserPage {
    WebDriver driver;
    @FindBy(linkText = "Logout")
    WebElement logoutlink;

    public UserPage (WebDriver driver)
    {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    //check we're on the right page
    public boolean checkCorrectPage() {
        return driver.getTitle().startsWith("Logged In");
    }

    //create a method to click on the LogOut link
    public void clickOnlogout() {
        logoutlink.click();
    }
}
